# Programming of Quantum computers
This is the Gitlab repository of the bachelor degree project by Vlastimil Hudeček at CTU FNSPE.

## What can be found here
This repository holds:
- the source code of the thesis text in LaTeX
- the most up to date version of the thesis in PDF
- Jupyter notebooks used for the implementations of the quantum algorithms
